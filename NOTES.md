# Notes

## References

- https://gitlab.com/gitlab-org/gitlab/-/issues/232157
- https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme

## Commands

```bash
cp ~/Documents/GitHub/joaopalmeiro/README.md ~/Documents/GitHub/joaommpalmeiro/README.md
```
